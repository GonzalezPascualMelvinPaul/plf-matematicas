## Conjuntos, Aplicaciones y funciones.
```plantuml
@startmindmap
*[#FBF4E9] Conjuntos aplicaciones y funciones.
	*_ Los
		*[#FFEDED] Conjuntos.
			*_ Se puede
				*[#E7EAB5] Definir
					*_ Como una
						*[#EFEFEF] Colección. 
						*[#EFEFEF] Reunión. 
			*_ Tipos.
				*[#E7EAB5] Vacío
					*_ Es un
						*[#EFEFEF] Conjunto de referencia\nen el que ocurre todas las\ncosas de las teorias.
				*[#E7EAB5] Universal
					*_ Es una
						*[#EFEFEF] Necesidad logica para\ncerrar bien las cosas.
					*_ No
						*[#EFEFEF] Tiene elementos.	
			*_ Algunos 
				*[#E7EAB5] Conceptos primitivos.
					*[#EFEFEF] Inclusión de conjuntos.
						*_ Es
							*[#FEFBF3] La primera nocion que\nhay entre dos conjuntos.
						*_ Esta
							*[#FEFBF3] Incluido en otro, cuando todos los\nelementos del primero pertence al segundo.
			*_ Las
				*[#E7EAB5] Operaciones
					*_ Son
						*[#EFEFEF] Intersección
							*_ Son los
								*[#FEFBF3] Elementos que pertenecen\nsimultaneamente a ambos.
						*[#EFEFEF] Union
							*_ Son los
								*[#FEFBF3] Elementos que pertencen al\nmenos algunos de ellos.
						*[#EFEFEF] Complementación
							*_ Son los que
								*[#FEFBF3] No pertenecen, complementario de un\nconjunto que no pertenecen a un conjunto dado.  
						*[#EFEFEF] Diferencia de conjuntos.
							*_ Es la
								*[#FEFBF3] Mezcla o resultado de hacer varias operaciones.
			*_ Las
				*[#E7EAB5] Representaciones graficas.
					*_ Ayudan
						*[#EFEFEF] A entender los conceptos y resolverlos.
					*_ Suelen
						*[#EFEFEF] Representarse mediante\nDiagramas de Venn
							*_ Es una idea
								*[#FEFBF3] De coleccion de elementos que forman parte\no tienen una caracteristica en comun.
							*_ Puede representarse
								*[#FEFBF3] Mediante un sistema
									*_ Como son un
										* Circulo
										* Ovalo
										* Lineas cerradas.
							*_ Se deben a
								*[#FEFBF3] Al lógico John Venn.
							*_ Ayuda a comprender
								*[#FEFBF3] Intuitivamente la posicion\ngraficamente sus operaciones.
			*_ El 
				*[#E7EAB5] Cardinal de un conjunto.
					*_ Es
						*[#EFEFEF] El número de elementos\nque conforman el conjunto.
							*_ Con los
								*[#FEFBF3] Numero naturales.
					*_ Sus
						*[#EFEFEF] Características.
							*[#FEFBF3] Fórmula cardinal de\nla union de conjuntos.
							*[#FEFBF3] Acotación de Cardinales.
										
			*_ La
				*[#E7EAB5] Imagen reversa\nde un subconjunto.
					*_ Una determinada.
						*[#EFEFEF] Transformación de la manera\nque sea gráfico mediante símbolos.
	*_ La
		*[#FFEDED] Transformación
			*_ Se le 
				*[#E7EAB5] Denomina aplicación o función.
			*_ Donde
				*[#E7EAB5] Se hay un proceso de cambio.
	*_ Las
		*[#FFEDED] Aplicaciones.
			*_ Una aplicación también
				*[#E7EAB5] Es una transformacion\no regla que convierte a cada uno\nde los conjuntos de los elementos.
			*_ De tipo.
				*[#E7EAB5] Inyectiva.
					*_ En donde
						*[#EFEFEF] Si dos elementos tienen la misma imagen,\nnecesariamente son el mismo elemento.
				*[#E7EAB5] Sobreyectiva.
					*[#EFEFEF] Si es equivalente.
				*[#E7EAB5] Biyectiva.
					*[#EFEFEF] Si es inyectiva y sobreyectiva.

	*_ Las
		*[#FFEDED] Funciones.
			*_ Como
				*[#E7EAB5] Sistema de coordenadas.
					*_ Son
						*[#EFEFEF] Grafics de función
							*_ De tipo
								*[#FEFBF3] Línea recta.
								*[#FEFBF3] Serie de puntos.
								*[#FEFBF3] Parábolas
			*_ Es
				*[#E7EAB5] Una terminología.
@endmindmap
```

## Funciones.
```plantuml
@startmindmap
*[#B2B8A3] Funciones.
	*_ Son
		*[#F4C7AB] Las transformaciones\nde un conjunto en otro.
	*_ La
		*[#F4C7AB] Representación cartesiana.
			*_ Inventada por
				*[#FFF5EB] Rene Descartes.
			*_ Representa
				*[#FFF5EB] Un plano los valores de los conjuntos de numeros.
			*_ Compuesta por 
				*[#FFF5EB] Ejes X y Y.
				*[#FFF5EB] Números reales.
			*[#FFF5EB] Curva en la grafica de la función.
				*_ Nos indica
					*[#FFF5EB] Como es la función.
					*[#FFF5EB] Como va cambiando.
	
	*_ De tipo
		*[#F4C7AB] Creciente.
			*_ Cuando
				*[#FFF5EB] Aumenta la variable independiente\ny las imagenes.
		*[#F4C7AB] Decreciente.
			*_ Cuando
				*[#FFF5EB] Disminuye la variable independiente\ny las imagenes.
	*_ Algunos
		*[#F4C7AB] Comportamientos
			*[#DEEDF0] Máximos.
				*_ Cuando
					*[#FFF5EB] Alcanza el mayor valor.
			*[#DEEDF0] Mínimos.
				*_ Cuando
					*[#FFF5EB] Alcanza el mínimo valor.
			*[#DEEDF0] Limites.
				*[#FFF5EB] Cunado su valor se aproxima a\nlos valores de una funcion.
			*[#DEEDF0] Continuidad.
				*_ Características
					*[#FFF5EB] No produce sobresalto\nni discontinuidades.
					*[#FFF5EB] Funciones mas manejables.
					*[#FFF5EB] Tienen un buen comportamiento.
			*[#DEEDF0] Discontinuidad.
				*_ Características
					*[#FFF5EB] No es continua.
					*[#FFF5EB] Presenta algún punto con salto.
			*[#DEEDF0] Intervalos.
				*_ Es un
					*[#FFF5EB] Trozo del rango de valores que se pueden tomar.
			*[#DEEDF0] Dominio.
				*_ Es un
					*[#FFF5EB] Conjunto de elementos X de variable independiente.
	*_ El
		*[#F4C7AB] Calculo diferencial.
			*_ Y la
				*[#DEEDF0] Derivada
					*_ Es
						*[#FFF5EB] Funcion simple con función complicada.
					*_ Su
						*[#FFF5EB] Objetivo es resolver el problema de\naproximacion de una funcion compleja.
							*_ A través
								*[#FFFFFF] De una función simple.
					*_ Se
						*[#FFF5EB] Utilizan calculos matematicos de reglas basicas.
					*_ Derivada de una
						*[#FFF5EB] Suma.
						*[#FFF5EB] Constante por funcion.
						*[#FFF5EB] Potencia entera positiva.
						*[#FFF5EB] Constante.
						*[#FFF5EB] Producto.
						*[#FFF5EB] Cociente.
						*[#FFF5EB] Funcion Trigonométricas.
					*_ Uso de
						*[#FFF5EB] La regla de la cadena.
					*_ Áreas.
						*[#FFF5EB] Economía.
						*[#FFF5EB] Ciencias sociales.
@endmindmap
```
## La matemática del computador.
```plantuml
@startmindmap
*[#798777] La matemática del computador.
	*_ Las
		*[#A2B29F] Matemáticas
			*[#A3D2CA] Ciencia que parte\nde dedución lógica.
			*[#A3D2CA] Estudia los valores abstractos.
			*[#A3D2CA] Aritmética finita.
				*_ Es la
					*[#FEFFDE] Aproximación de un número.
						*_ Cuando 
							*[#F6F6F6] Se aproxima a un numero real\no fraccionario con un numero\nfinito de cifras.
				*_ Con
					*[#FEFFDE] Infinitas cifras.
						*_ De números.
							*[#F6F6F6] Reales.
							*[#F6F6F6] Fraccionales.
							*[#F6F6F6] Decimales.
				*_ Con técnicas.
					*[#FEFFDE] Truncamiento.
						*_ Se
							*[#F6F6F6] Desprecia un determinado numero de cifras.
					*[#FEFFDE] Redondeo.
						*_ Se
							*[#F6F6F6] Desprecia y retoca la ultima\ncifra que se desprecia de modo adecuado.
				*_ Con
					* Cifras significativas.
						*_ Representa
							*[#F6F6F6] El uso de una o mas escalas\nde incertidumbre en determinadas aproximaciones.
				*_ La
					*[#FEFFDE] Aritmética de tipo flotante.
						*_ Se 
							*[#F6F6F6] Suma, multiplica, resta números.
								*_ Representados.
									* En condición científica.
				*_ La
					*[#FEFFDE] Representación interna.
						*[#F6F6F6] Convierte un paso o no\nde corriente  de 0 y 1.
						*[#F6F6F6] Números enteros.
							*_ De representación.
								* Magnitud de signo.
								* En exceso.
								* En complemento a 2.
				*_ Representan.
					*[#FEFFDE] Números grandes o pequeños.
						*_ Con
							*[#F6F6F6] Forma de potencia de 10.
								*_ Nos
									* Genera una idea de la magnitud del numero.
									* Conserva un orden de número.
			
			*[#A3D2CA] Resuelve problemas cotidianos.
			*[#A3D2CA] Usado en el desarrollo de computadoras.
	*_ El
		*[#A2B29F] Sistema utilizado.
			*_ De tipo
				*[#A3D2CA] Octal.
					*[#FEFFDE] Base aritmética de número 8.
					*[#FEFFDE] Utilizado en la informática.
				*[#A3D2CA] Hexadecimal.
					*[#FEFFDE] Posicional
					*[#FEFFDE] Utiliza de base 16.
				*[#A3D2CA] Decimal.
					*[#FEFFDE] Base aritmética el numero 10.
					*[#FEFFDE] Representación completa de número.
				*[#A3D2CA] Binario.
					*_ Representación
						*[#FEFFDE] Números (0 y 1).
						*[#FEFFDE] Enlaza lógica booleana.
						*[#FEFFDE] Magnitud de signo.
						*[#FEFFDE] Numero enteros.
					*_ Con 
						*[#FEFFDE] Dígitos binarios.
						*[#FEFFDE] Símbolos.
					*_ Operación.
						*[#FEFFDE] suma binaria.
					*_ Usos.
						*[#FEFFDE] Videos.
						*[#FEFFDE] Aspectos digitales.
	*_ El
		*[#A2B29F] Ordenador.
			*_ Representación
				*[#A3D2CA] De números.
					*_ Con
						*[#F6F6F6] Codificación Binaria.
@endmindmap
```
